# Test tasks for QA Engineer - Denis Genov solution

## Task 1
See `task1/ANSWERS.md`

## Task 2
I've chose [deckofcards](https://github.com/crobertsbmw/deckofcards) from the recommended [list](https://github.com/toddmotto/public-apis).
It does not require authorization.

Test cases are described in `task2/tests/test_cases.md`.

### Getting Started

You will need Python 3.6+ for running those tests. 
They may work on earlier versions, but this was not tested.

You need to install `pytest` and `requests`.
> If you use virtual environment make sure it is activated before running below commands.

```
pip install pytest
pip install requests
```

### Running the tests

**You can view build results in the Pipelines section. No need to run on your side.**

To run on your side:

0. Clone current repository
1. Open console app
2. Change your working directory to `task2` dir
3. Run ```pytest -v --tb=short```

Some tests will fail. That is OK. I believe there are some issues with the API.


### What should be improved
1. Logging. Make it more consistent.
2. Parametrization - there is no need to run each test with 2 variants - 
"jokers on" and "jokers off". They are mostly needed for the case of drawing 
all the cards in deck at ones. I've added it just to show that I know that 
parametrization can be made several times for one test.
3. Log the random seed and add an option to pass it to the test - to be able 
to rerun failed tests with the same input.
4. Add requirements.txt
5. Rework docstrings so that they could be used to generate documentation
(sphinx/pydoc/doxygen/etc. - depending on the team preferences)
 

## Authors

**Denis Genov** - [denisgenov@gmail.com](mailto:denisgenov@gmail.com)

