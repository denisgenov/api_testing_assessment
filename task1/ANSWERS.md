> Imagine the following situation: you need to establish a QA process in a 
> cross-functional team. The team builds a front-end application using REST
> APIs.

> **Q1: Where would you start? What would be your first steps?**

1. Get familiar with the product/specifications as close as possible.
 Understand the system under test. Consult with product owners.

2. Find out the competencies of all team members. Choose the most suitable 
 testing tools, taking into account the experience of team members. Make sure 
 everyone understand their role and impact. Synchronize with the team at the 
 beginning, if necessary - plan team education. Subsequently, the whole
 team must move forward together. Hold rallies periodically in order to 
 eliminate the out of sync. 

3. Create test cases for the core functionality.

4. Automate core functionality test cases. Develop related part of the test lib
 in parallel.
 
5. After some basic automation is done - take a break in writing tests and step 
 aside. Review project structure, library, etc. At this point the QA team 
 should have a vision of what is going right and what could be improved. Make 
 needed architectural changes to avoid painful refactoring at later stages. 
 Make further work comfortable. Establish requirements for the process and 
 tests code. Document those requirements and follow them.

6. Proceed automation incrementally, with periodical sync with the team and 
 managers.


> **Q2: Which process would you establish around testing new functionality?
> How would you want the features to be tested?**

For each new feature:
1. Write automated tests.

2. Integrate the tests into CI.

3. Set hooks for pull requests.
 Auto tests should be triggered on each pull-request before review and merge.

4. In case of mandatory heavy tests and/or big number of tests + limited
 infrastructure - split tests into multiple tiers.
 Set the testing lifecycle depending on the tiers characteristics. 
 E.g. run "SMOKE + L1" tests on pull requests, L2 - daily, L3 - weekly.


> **Q3: If you would do test automation which techniques or best practices
> would you use?**

- Choose popular tools with big community and under active development,
 such as python + pytest + requests + git + jenkins.
- Move the complexity from tests to the lib. Tests code should be clean and
 readable.
- Commit and follow a unified coding style across the project.
- Make the code review mandatory.
- Document each test.
- Tests should not rely on the current infrastructure. It should be easy to run
 them in other environment - on customer's side or at cloud.