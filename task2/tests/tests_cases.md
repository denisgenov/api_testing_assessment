# TEST CASES
## DRAW request CASES
### Overview:
Draw request looks like:

```curl -X GET https://deckofcardsapi.com/api/deck/<deck_id>/draw/?count=<count>```

It takes two parameters:

* `deck_id` - an unique identifier of deck of cards, which is obtained upon new deck request
* `count` - number of cards to be drawn (_when dealing with `count` one should take into account how many cards are left in the deck_)

Possible variations for `deck_id`:

* positive case: syntactically correct value, corresponding deck is present in the server database
* negative cases: all others; (_I will not focus on this parameter in the scope of current task_)

Variations for `count` values:

* Syntactically and semantically correct:
    * no value (default = 1 should be used in the case)
    * value is `X`, such that `0 <= X <= num cards in deck` 
* Semantically incorrect:
    * value is `X`, such that `X < 0` or  `X > num cards in deck`
* Syntactically incorrect: non-numerical


I'll focus on testing the `count` parameter variations.
Below are two cases descriptions - one positive and one negative.

### Case 1 (positive)
#### Test_name:
`test_draw_from_full_deck_positive[count='None'-no_jokers]`

#### Test_ID:
Randomly generated `<id>`, e.g. `425cc689-569b-4511-887e-752a2199ddab`.

_I prefer this approach because it does not depend on test name, thereby it will survive changes in test name._

#### Description:
Draw from full deck without jokers with missing `count` parameter should draw 1 card.

#### Pre-conditions:
New deck without jokers was requested, `deck_id` is obtained, deck consists of 52 cards.

#### Steps:

1. Send GET request:
`https://deckofcardsapi.com/api/deck/<deck_id>/draw/`

#### Expected result:

* Response code: 200
* Response data:
    * 'success' = 'true'
    * 'deck_id' matches requested
    * 'remaining' = 51
    * 'cards' list consists of 1 element

#### Post-conditions:
Deck should consist of 51 cards. Check "remaining" value in response for GET request:
`https://deckofcardsapi.com/api/deck/<deck_id>/`


### Case 2 (negative)

#### Test_name:
`test_draw_from_full_deck_negative[count='>cards in deck'--no_jokers]`

#### Test_ID:
`eaae9abe-8601-4111-8740-968d775a8284` (randomly generated)

#### Description:
Draw request from full deck without jokers with `count` value > `number of cards in deck`
(e.g. '100') parameter should fail and not change the deck.

#### Pre-conditions:
New deck without jokers was requested, `deck_id` is obtained, deck consists of 52 cards.

#### Steps:

1. Send GET request:
`https://deckofcardsapi.com/api/deck/<deck_id>/draw/?count=100`

#### Expected result:
* Response code: 400 or 423

OR

* Response code: 200
* Response data:
    * 'success' = 'false'
    * 'deck_id' matches requested
    * 'remaining' = 52
    * 'cards' list consists of 0 elements

#### Post-conditions:
* Deck should consist of 52 cards. Check "remaining" value in response for GET request:
 `https://deckofcardsapi.com/api/deck/<deck_id>/`
