import logging
import itertools

from testlib import api


CARD_SUITS = {
    'S': 'SPADES',
    'D': 'DIAMONDS',
    'C': 'CLUBS',
    'H': 'HEARTS',
}
CARD_STD_VALUES = {
    'A': 'ACE',
    '2': '2',
    '3': '3',
    '4': '4',
    '5': '5',
    '6': '6',
    '7': '7',
    '8': '8',
    '9': '9',
    '0': '10',
    'J': 'JACK',
    'Q': 'QUEEN',
    'K': 'KING',
}
ALLOWED_STD_CODES = [
    ''.join(p) for p in itertools.product(
        CARD_STD_VALUES.keys(), CARD_SUITS.keys())
]

JOKER_VALUES = {'X': 'JOKER'}
JOKER_SUITS = {'1': 'BLACK', '2': 'RED'}
JOKER_CODES = [
    ''.join(p) for p in itertools.product(
        JOKER_VALUES.keys(), JOKER_SUITS.keys())
]

ALLOWED_CODES = ALLOWED_STD_CODES + JOKER_CODES


class Card:
    """
    Class representing cards with attributes matching those from API.
    """
    def __init__(self, code, value=None, suit=None, images=None, image=None):
        """
        Initialize a new Card Object.

        :param code: str, card code (mandatory)
        :param value: str
        :param suit: str
        :param images: dict
        :param image: str
        """
        if code not in ALLOWED_CODES:  # Allow creation of invalid cards
            logging.warning(f"'{code}' is a non-acceptable card code")
        self.code = code
        self.value = value or self.code[0]
        self.suit = suit or self.code[1]
        self.images = images or {}
        self.image = image


class Deck:
    """
    Class for representing decks and related API methods.

    FIXME: Only methods required for test_draw are implemented.
    """
    def __init__(self, deck_id, remaining=None, shuffled=None, cards=None):
        self.deck_id = deck_id
        self.remaining = remaining
        self.shuffled = shuffled
        self.cards = [] if cards is None else cards

    @classmethod
    def new(cls, cards=None, jokers_enabled=False):
        """
        Request a new deck and return object describing the new deck.

        :param cards: str|list(str)|tuple(str), containing card codes
         which should make up the new deck
        :param jokers_enabled: bool, if True - the new deck will contain
         Jokers; this parameter is ignored if `cards` is specified
        :return: new Deck Object
        """
        logging.info(f"Request new Deck(cards={cards}, jokers_enabled={jokers_enabled})")
        if cards and jokers_enabled:
            logging.warning("'jokers_enabled' option is ignored when a set of cards is specified")
        if isinstance(cards, list) or isinstance(cards, tuple):
            cards = ','.join(cards)
        resp = api.get_new_deck(cards=cards, jokers_enabled=jokers_enabled)
        resp.raise_for_status()

        data = resp.json()
        success = data['success']
        if not success:
            raise api.API_URL('API responded FAIL upon request for a New Deck')

        if cards:
            card_codes = cards
        else:
            card_codes = ALLOWED_CODES if jokers_enabled else ALLOWED_STD_CODES

        obj = cls(data['deck_id'])
        obj.remaining = data['remaining']
        obj.shuffled = data['shuffled']
        obj.cards = [Card(card_code) for card_code in card_codes]
        return obj

    def draw(self, count=None):
        """
        Call `draw` api method. Raise Exception if request failed.

        :param count: int, number of cards to be drawn
        :return: dict, parsed json response
        """
        logging.info(f"Draw {count} cards from deck {self.deck_id}")
        resp = api.draw_cards(self.deck_id, count)
        resp.raise_for_status()
        data = resp.json()
        return data

    def get_info(self):
        """
        Request info about deck from server.

        :return:  dict, parsed json response
        """
        logging.info(f"Request info for deck {self.deck_id}")
        resp = api.get_deck_info(self.deck_id)
        resp.raise_for_status()
        data = resp.json()
        return data
