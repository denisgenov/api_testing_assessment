"""
Collection of functions to work with deckofcardsapi API.

Arguments passed to functions are not checked for correctness.
Response codes are not checked.
This is done to enable negative test scenarios.

FIXME: Only methods required for test_draw are implemented.
"""

import requests


SITE_URL = 'https://deckofcardsapi.com/'
API_URL = SITE_URL + 'api/'

DEFAULT_DRAW_COUNT = 1


def get_new_deck(cards=None, jokers_enabled=False):
    """
    Request a new deck.

    :param cards: str, representing list of comma-separated card codes
     which should make up the new deck
    :param jokers_enabled: bool or str, if True - the new deck will contain
     Jokers; this parameter is ignored if `cards` is specified
    :return: requests.Response Object representing server response
    """
    params = {}
    if cards:
        params['cards'] = cards
    if jokers_enabled is not False:  # Allow requests with non-boolean arguments
        params['jokers_enabled'] = 'true' if jokers_enabled is True else jokers_enabled

    return requests.get(API_URL + 'deck/new/', params=params)


def get_deck_info(deck_id):
    """
    Request info about deck with `deck_id`.

    :param deck_id: str, unique id of deck from which cards will be drawn
    :return: requests.Response Object representing server response
    """
    return requests.get(API_URL + f'deck/{deck_id}/')


def draw_cards(deck_id, count=None):
    """
    Draw `count` cards (or DEFAULT_DRAW_COUNT) from deck with `deck_id`.

    :param deck_id: str, unique id of deck from which cards will be drawn
    :param count: int, number of cards to be drawn
    :return: requests.Response Object representing server response
    """
    return requests.get(API_URL + f'deck/{deck_id}/draw/', params={'count': count})


# TODO: Implement the rest of API functions.
#  Refer to SITE_URL for more details.
