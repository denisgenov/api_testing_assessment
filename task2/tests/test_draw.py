import pytest
import logging
import random
import string
import requests

import testlib


@pytest.mark.parametrize(
    'jokers_enabled', [False, True], ids=["no_jokers", "with_jokers"]
)
@pytest.mark.parametrize(
    'count', [
        random.randint(1, len(testlib.deck.ALLOWED_STD_CODES) - 1),  # value between boundaries
        None, 0, len(testlib.deck.ALLOWED_STD_CODES),  # boundary values
    ],
    ids=lambda count: f"count='{count}'"
)
def test_draw_from_full_deck_positive(count, jokers_enabled):
    """
    Test positive scenarios for draw request from full deck.

    To be able to process corner cases we adjust the `count` parameter:
    - If `jokers_enabled` is True, then count is increased by the number of
        testlib.deck.JOKER_CODES elements. This allows us to request draw for
        all cards in deck.
    - If `count` is None - an empty draw request is sent to test the default
        value (testlib.api.DEFAULT_DRAW_COUNT).
    """
    if count is None:
        expected_draw_count = testlib.api.DEFAULT_DRAW_COUNT
    elif jokers_enabled and count == len(testlib.deck.ALLOWED_STD_CODES):
        count += len(testlib.deck.JOKER_CODES)
        expected_draw_count = count
    else:
        expected_draw_count = count

    deck = testlib.deck.Deck.new(jokers_enabled=jokers_enabled)
    logging.info(f"working with a new deck: deck_id = {deck.deck_id}")

    expected_left_count = deck.remaining - expected_draw_count
    data = deck.draw(count)

    assert data['success'], "response reported fail but success is expected"
    assert data['deck_id'] == deck.deck_id, "response contains wrong deck_id"
    assert data['remaining'] == expected_left_count, \
        "'remaining' value is wrong"
    assert len(data['cards']) == expected_draw_count, \
        "draw returned list with wrong number of cards"

    updated_deck_info = deck.get_info()
    assert updated_deck_info['remaining'] == expected_left_count, \
        "wrong number of cards in deck after draw request was processed"


@pytest.mark.parametrize(
    'jokers_enabled', [False, True], ids=["no_jokers", "with_jokers"]
)
@pytest.mark.parametrize(
    "count, acceptable_codes", [
        # negative scenario: try to draw negative number of cards
        (random.randint(-100, -1),
         [requests.codes['bad_request'],
          requests.codes['unprocessable_entity'],
          ]),
        # negative scenario: try to draw more cards than available
        (len(testlib.deck.ALLOWED_CODES) + random.randint(1, 100),
         [requests.codes['bad_request'],
          requests.codes['unprocessable_entity'],
          ]),
        # negative scenario: `count` has a non-numeric value
        (random.choice(string.ascii_letters + string.punctuation),
         [requests.codes['bad_request'],
          ]),
    ],
    ids=lambda param: f"count='{param}'" if not isinstance(param, list) else ''
)
def test_draw_from_full_deck_negative(count, jokers_enabled, acceptable_codes):
    """
    Test negative scenarios for draw from full deck.

    PASS criteria:
    - fail-indicating status code is expected or api should report
        'success'=false
    - data should not be changed on server side

    README:
    1. Assertions reflect my vision. It may not match the API author's vision.
        As far as I see, the data is changed on server side and the API does
        not handle negative scenarios. Thus, negative scenarios fail.

    2. Seems like support for negative `count` values is a non-documented API
        feature - if negative value is provided, it indicates how many cards
        should left in the deck after draw. I've discovered this after the test
        was ready. I will not change the test because it still allows you to
        evaluate my programming skills. BTW, I'd prefer to have a separate
        parameter for such cases, e.g.`leave_count`.
    """
    deck = testlib.deck.Deck.new(jokers_enabled=jokers_enabled)
    logging.info(f"working with a new deck: deck_id = {deck.deck_id}")

    resp = testlib.api.draw_cards(deck.deck_id, count)
    logging.info(f"Response status code = {resp.status_code}")
    if resp.status_code not in acceptable_codes:
        # API is designed so that it has a boolean variable in its responses
        #  to indicate if the request was processed successfully. I disagree
        #  with this approach, but let's honor the author. Here we check the
        #  answer if response status code foes not match our expectations.

        # 'is False' - to catch invalid values, e.g. - empty string, or zero
        assert resp.ok and resp.json()['success'] is False, \
            "negative scenario is not handled by the server"
        data = resp.json()
        assert data['deck_id'] == deck.deck_id, \
            "response contains wrong deck_id"
        assert data['remaining'] == deck.remaining, \
            "'remaining' value changed after incorrect draw request"
        assert not data['cards'], \
            "incorrect draw request returned non-empty list of cards"

    updated_deck_info = deck.get_info()
    assert updated_deck_info['remaining'] == deck.remaining, \
        "wrong number of cards in deck after draw request was processed"
